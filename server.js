const express = require('express');
const fs = require('fs');
const http = require('http');
const https = require('https');
const cors = require('cors');
const bodyParser = require('body-parser');
const mysql = require('mysql');
const webpush = require('web-push');
const needle = require('needle');

const privateKey  = fs.readFileSync('sslcert/server.key', 'utf8');
const certificate = fs.readFileSync('sslcert/server.crt', 'utf8');
const credentials = {key: privateKey, cert: certificate};

const vapidKeys = {
  publicKey: 'BCzk_La540h0JYvp_aco7affvZvRrqB7YRgnXw4t9kb_S83Iq6ff238hOlG6Yr23HpoYUHOCjD-rA8Fzntnqkw0',
  privateKey: 'IOQHRkTulmxYFyTuq5mMMIG3wD9sfjsKZQbSXgoZMuE'
};

const corsOptions = {
  origin: '*',
  optionsSuccessStatus: 200
};

const db = mysql.createConnection({
  host: '',
  user: '',
  password: '',
  database: ''
});

let SUBSCRIPTIONS = [];

db.connect((error) => {
  if (error) throw error;
  console.log('Database was connected!');
  db.query('SELECT * FROM subscriptions', (error, result) => {
    if (error) throw error;
    result.map(el => {
      SUBSCRIPTIONS.push(JSON.parse(el.subscriber));
    });
  });
});

webpush.setVapidDetails(
    'mailto:ignatrychkov@gmail.com',
    vapidKeys.publicKey,
    vapidKeys.privateKey
);

const onlinerUrl = 'https://ak.api.onliner.by/search/apartments?';

const app = express();

app.use(cors(corsOptions));
app.use(bodyParser.json());
const httpServer = http.createServer(app);
const httpsServer = https.createServer(credentials, app);

httpServer.listen(7777, () => {
  console.log('HTTP server started!');
});

httpsServer.listen(8888, () => {
  console.log('HTTPS server started!');
});

app.route('/api/apartments/search').get((request, result) => {
  needle.get(onlinerUrl + request._parsedUrl.query, function (error, response) {
    if (error) {
      result.status(500).json({message: 'Server Error'});
    } else {
      result.status(200).json(response.body);
    }
  });
});

app.route('/api/push/subscribe').post((request, result) => {
  const subscriber = request.body;
  db.query(`INSERT INTO subscriptions (subscriber) VALUES ('${JSON.stringify(subscriber)}')`, (error, response) => {
    if (error) throw error;
    console.log(response);
    SUBSCRIPTIONS.push(subscriber);
    result.status(200).json([]);
  });
});

app.route('/api/push/send').get((request, result) => {
  // sample notification payload
  const notificationPayload = {
    'notification': {
      'title': 'Test Push',
      'body': 'Push messages are available now!',
      'icon': 'assets/icons/icon-512x512.png',
      'vibrate': [100, 50, 100],
      'data': {
        'dateOfArrival': Date.now(),
        'primaryKey': 1
      },
      'actions': [{
        'action': 'explore',
        'title': 'Go to the site'
      }]
    }
  };
  Promise.all(SUBSCRIPTIONS.map(subscriber => webpush.sendNotification(
      subscriber, JSON.stringify(notificationPayload) )))
  .then(() => result.status(200).json({message: 'Push messages sent successfully.'}))
  .catch(error => {
    if (error.statusCode === 410) {
      db.query(`DELETE FROM subscriptions WHERE subscriber LIKE '%${error.endpoint}%'`, (error) => {
        if (error) throw error;
      });
    } else {
      console.error('Error sending notification, reason: ', error);
    }
  });
});